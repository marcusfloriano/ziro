var express = require('express');
var router = express.Router();
var passport = require('../utils/passport-util');
var User = require('../models/user');
var debug = require('debug')('ziro:routes:index');
var aclUtil = require('../utils/acl-util');

/* GET home page. */
router.get('/', aclUtil.acl.middleware(), function(req, res, next) {
	res.render('index');
});

router.get('/signin', function(req, res, next) {
	res.render('./sign/signin', { req: req } );
});

router.post('/signin', passport.authenticate('local', {
	successRedirect: '/',
	failureRedirect: '/signin',
	failureFlash: true
}));

router.get('/signup', function(req, res, next) {
	res.render('./sign/signup' );
});

router.post('/signup', function(req, res, next) {
	debug("Signup new user");
	User.register(new User({email: req.body.email}), req.body.password, function(err, user) {
		if(err) {
			debug('Erro on register new user: %s', req.body.email);
			req.flash('error',req.i18n.t("sign:"+err.message));
			res.redirect('/signup');
		} else {
			debug('New user registred!');
			debug(user);
			aclUtil.acl.allow(user.id,'/users/' + user.id ,'get');
			aclUtil.acl.allow(user.id,'/users/' + user.id ,'post');
			aclUtil.acl.allow('guest','/','get');
			aclUtil.acl.addUserRoles(user.id,user.id);
			aclUtil.acl.addUserRoles(user.id,'guest');
			res.redirect('/');
		}
	});
});

router.get('/signout',function(req, res, next) {
	req.logout();
	req.session.userId = null;
	res.redirect('/');
});

module.exports = router;
