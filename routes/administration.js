var debug = require('debug')('ziro:routes:administration');
var express = require('express');
var router = express.Router();
var aclUtil = require('../utils/acl-util');
var User = require('../models/user');
var async = require('async');

router.get('/', aclUtil.acl.middleware(), function(req, res, next) {
	res.render('./administration/index');
});

router.get('/users', aclUtil.acl.middleware(), function(req, res, next) {
	var roles_of_user = [];
	User.find({}, function(err, users) {
		async.each(users, function(user, callback) {
			aclUtil.acl.userRoles(user.id,function(err, roles) {
				if(err) callback(err);
				roles_of_user[user.id] = roles;
				callback();
			});
		}, function(err) {
			if(err) return next(err);
			console.log(roles_of_user);
			res.render('./administration/users', {users: users, roles_of_user: roles_of_user} );
		});
	});
});

router.post('/users/:id/roles/:role', aclUtil.acl.middleware(2), function(req, res, next) {
	if(req.body._method == "DELETE") {
		aclUtil.acl.removeUserRoles(req.params.id, req.params.role, function(err){
				if(err) return next(err);
				res.redirect("/administration/users");
		});
	} else if(req.body._method == "PUT") {
		aclUtil.acl.addUserRoles(req.params.id, req.params.role, function(err){
				if(err) return next(err);
				res.redirect("/administration/users");
		});
	}
});

module.exports = router;
