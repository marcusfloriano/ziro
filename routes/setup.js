var debug = require('debug')('ziro:routes:setup');
var express = require('express');
var router = express.Router();
var aclUtil = require('../utils/acl-util');
var User = require('../models/user')

/* GET home page. */
router.get('/', function(req, res, next) {

  aclUtil.acl.allow('guest','/','get');
  aclUtil.acl.allow('administration','/administration','*');
  aclUtil.acl.allow('administration','/administration/users','*');
  aclUtil.acl.allow('administration','/administration/roles','*');

  User.findOne({email:"admin@ziro.com.br"}, function(err, user){
    if(err) return next(err);
    if(user != null) {
      debug('Update permission for admin user!');
      aclUtil.acl.allow(user.id,'/users/' + user.id , '*');
      aclUtil.acl.addUserRoles(user.id,user.id);
      aclUtil.acl.addUserRoles(user.id,'guest');
      aclUtil.acl.addUserRoles(user.id,'administration');
      res.send("update permission");
    } else {
      User.register(new User({email: "admin@ziro.com.br"}), "admin", function(err, user) {
    		if(err) {
    			debug('Erro on register new user: %s', req.body.email);
    			req.flash('error',err.message);
          res.send("err");
    		} else {
    			debug('New user registred!');
          aclUtil.acl.allow(user.id,'/users/' + user.id , '*');
          aclUtil.acl.addUserRoles(user.id,user.id);
          aclUtil.acl.addUserRoles(user.id,'guest');
          aclUtil.acl.addUserRoles(user.id,'administration');
          res.send("add admin user ok");
    		}
    	});
    }
  });
});

module.exports = router;
