var debug = require('debug')('ziro:routes:products');
var express = require('express');
var router = express.Router();
var aclUtil = require('../utils/acl-util');

/* GET home page. */
router.get('/', aclUtil.acl.middleware(), function(req, res, next) {
	res.render('./product/index');
});

module.exports = router;
