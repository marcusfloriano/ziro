var express = require('express');
var aclUtil = require('../utils/acl-util');
var router = express.Router();
var User = require('../models/user')
var debug = require('debug')('ziro:routes:users');

/* GET users listing. */
router.get('/:id', aclUtil.acl.middleware(), function(req, res, next) {
	res.render('./user/edit',{ req: req });
});

router.post('/:id', aclUtil.acl.middleware(), function(req, res, next) {
	debug("init alter user");
	if(req.body.name === undefined || req.body.name === "") {
		debug("The name of user not defined");
		req.flash('warning',req.i18n.t("users:Please, define your name, thank's"));
		res.redirect('/users/'+req.user.id);
		return next;
	} else {
		debug("Verify if user exists");
		User.findById(req.user.id,function(err, user){
			if(err) {
				debug("User not exists");
				req.flash('warning',req.i18n.t("users:Sorry, the user ID not exists: {{ID}}.", {ID: req.user.id}));
				res.redirect('/users/'+req.user.id);
				return next;
			} else {
				debug("User exists, set new values");
				user.name = req.body.name;
				user.save(function(err){
					debug("Save users with new values");
					if(err) return handleError(err);
					debug("Redirect for page profile of user");
					req.flash('success',"users:User saved with success");
					res.redirect('/users/'+req.user.id);
					return next;
				});
			}
		});
	}
});
module.exports = router;
