/**
* This add action for change locale
*/
$("#locales a").click(function(){
	var lang = $(this).attr("href").replace("#","");
	Cookies.set("i18next",lang);
	window.location.href=window.location.href;
	return false;
});