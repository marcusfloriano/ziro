var debug = require('debug')('ziro:app');
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session')
var flash = require('connect-flash');
var aclUtil = require('./utils/acl-util');
var i18nextUtil = require('./utils/i18next-util');
var passport = require('./utils/passport-util');
var SessionStore = require('connect-mongo')(session);
var mongoose = require('./utils/mongoose-util');

// routes required
var routes = require('./routes/index');
var users = require('./routes/users');
var products = require('./routes/products');
var administration = require('./routes/administration');
var setup = require('./routes/setup');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('node-sass-middleware')({
	src: path.join(__dirname, 'public'),
	dest: path.join(__dirname, 'public'),
	indentedSyntax: true,
	sourceMap: true,
	debug: false
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(i18nextUtil.i18nextMiddleware.handle(i18nextUtil.i18next, {
  ignoreRoutes: [],
  removeLngFromUrl: false
}));

app.use(session({
	secret: 'zirotheagiletoolssecrettokensession',
	cookie: { maxAge: 2628000000 },
	resave: false,
	saveUninitialized: false,
	store: new SessionStore({mongooseConnection: mongoose.connection})
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(flash());

// add common variables used for system
app.use(function(req, res, next){
	if(req.user) {
		//use in template
		res.locals.user = req.user;
		//used for check authentication
		req.session.userId = req.user.id;
	}
	//messages of flash for using in jade tamplate
	res.locals.flash_danger = req.flash('danger').concat(req.flash('error'));
	res.locals.flash_warning = req.flash('warning');
	res.locals.flash_success = req.flash('success');
	res.locals.flash_info = req.flash('info');

	res.locals.contains = function(str,regexp){
		var re = new RegExp(regexp);
		return re.test(str);
	}

	res.locals.originalUrl = req.originalUrl;
	debug(req.originalUrl);

	app.locals.inspect = require('util').inspect;

	next();
});

app.use('/', routes);
app.use('/users', users);
app.use('/products', products);
app.use('/administration', administration);
app.use('/setup', setup);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// this check error of ACLs and redirect correct
app.use(function(err, req, res, next) {
	debug("Check Error of ACLs - errorCode: " + err.errorCode + ", message: " + err.message);
	if(err.errorCode === 401 && err.message === "User not authenticated") {
		debug("This error is 401, redirect call");
		res.redirect("/signin");
	} else if(err.errorCode === 403 && err.message === "Insufficient permissions to access resource") {
		debug("This error is 403, for insufficient permission");
		aclUtil.acl.isAllowed(req.user.id, "/", "get", function(err_acl, allowed){
			if(err_acl){
				req.flash('danger', err.message);
				res.redirect("/signin");
			}
			if(allowed){
				req.flash('danger', err.message);
				res.redirect("/");
			} else {
				req.flash('danger', err.message);
				res.redirect("/signup");
			}
		});
	} else {
		debug("Not erro from ACLs, call next(err)");
		return next(err);
	}
});

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
		if(/text\/plain/.test(req.get('accept'))) {
	    res.send(err.stack);
		} else {
			res.render('error', {
	      message: err.message,
	      error: err
	    });
		}
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
	if(/text\/plain/.test(req.get('accept'))) {
		res.send(err.message);
	} else {
		res.render('error', {
	    message: err.message,
	    error: {}
	  });
	}
});


module.exports = app;
