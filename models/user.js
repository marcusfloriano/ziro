var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var User = new Schema({
	email: String,
	name: String
});

User.plugin(passportLocalMongoose, {
	usernameField: "email",
	usernameLowerCase: true
});

module.exports = mongoose.model('User', User);
