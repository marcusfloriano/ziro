var passport = require('passport');
var debug = require('debug')('ziro:utils:passport-util');
var User = require('../models/user');

passport.use(User.createStrategy());

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

module.exports = passport;
