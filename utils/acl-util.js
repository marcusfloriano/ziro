var debug = require('debug')('ziro:utils:acl');
var mongoose = require('../utils/mongoose-util');
var User = require('../models/user');
var acl = require('acl');
var async = require('async');

acl = new acl(new acl.mongodbBackend(mongoose.connection.db, "acl_"), { debug: debug });

exports.acl = acl;

var getAcls = function(email,callback) {
  User.findOne({email: email},function(err, user) {
    if(err) return callback(null, err);
    acl.userRoles(user.id,function(err, roles) {
      if(err) return callback(null, err);

      var acls = {
        "administration": {
          "/administration": {"*":false, "get":false, "post":false, "put":false, "delete":false},
          "/administration/users": {"*":false, "get":false, "post":false, "put":false, "delete":false}
        },
        "products": {
          "/products": {"*":false, "get":false, "post":false, "put":false, "delete":false}
        },
      };

      async.each(roles, function(role, callback) {
        async.forEachOf(acls[role],function(perms,resource,callback) {
          async.forEachOf(perms,function(active,perm,callback) {
            acl.isAllowed(user.id,resource,perm,function(err, allowed){
              if(err) return callback(err);
              if(allowed){
                acls[role][resource][perm] = true;
              }
              callback();
            });
          }, function(err) { callback(); });
        }, function(err) { callback(); });
      }, function(err) { callback(acls); });

    });
  });

}

exports.getAcls = getAcls;

var setAcls = function(email, value, checked, callback) {
  debug("setAcls " + email + ", " + value + ", " + checked);
  User.findOne({email: email},function(err, user) {
    if(err) return callback(null, err);

    var values = value.split(">");
    debug("acl.allow: " + values[0] + ", " + values[1] + ", " + values[2]);

    if(checked) {
      acl.addUserRoles(user.id,values[0]);
      acl.allow(values[0],values[1],values[2]);
      callback();
    } else {
      if(values[2] == "*") {
        acl.removeUserRoles(user.id,values[0]);
        acl.removeAllow(values[0],values[1],values[2]);
      }
      callback();
    }
  });
}

exports.setAcls = setAcls;
