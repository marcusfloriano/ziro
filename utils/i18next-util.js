var i18nextMiddleware = require('i18next-express-middleware');

exports.i18nextMiddleware = i18nextMiddleware;

var i18next = require('i18next');
var i18nBackend = require('i18next-node-fs-backend');
i18next
	.use(i18nBackend)
	.use(i18nextMiddleware.LanguageDetector)
	.init({
		debug: false,
		lng: "en",
	    fallbackLng: "en",
	    ns: [
	      "sign",
	      "common",
		  "users"
	    ],
	    "defaultNS": "common",
	    "fallbackNS": "common",
		backend: {
			loadPath: "./locales/{{lng}}/{{ns}}.json"
		},
		detection: {
			// order and from where user language should be detected
			order: [/*'path', 'session', */ 'querystring', 'cookie', 'header'],

			// keys or params to lookup language from
			lookupQuerystring: 'lng',
			lookupCookie: 'i18next',
			lookupSession: 'lng',
			lookupPath: 'lng',
			lookupFromPathIndex: 0,

			// cache user language
			caches: false, // ['cookie']

			// optional expire and domain for set cookie
			cookieExpirationDate: new Date(),
			cookieDomain: 'ziro.com.br'
		}
	});

exports.i18next = i18next;

exports.t = (key, options) => i18next.t(key, options);
