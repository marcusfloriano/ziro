var config = require('../config');

var mongoose = require('mongoose');
mongoose.connect(config.db, function(err){
	if(err) {
		console.log("Could not connect to mongodb on %s.", config.db);
	}
});

module.exports = mongoose;